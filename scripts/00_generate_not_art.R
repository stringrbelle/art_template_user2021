library(ggplot2)

size = c(6.7, 4.07)

ggplot() +
  annotate("text", x = 1, y = 1, label = "?", size = 30) +
  theme_void() +
  theme(plot.background = element_rect(fill = 'red'))

ggsave("figs/art/Jane_Doe_01.png", width = size[1], height = size[2])

ggplot() +
  annotate("text", x = 1, y = 1, label = "here::i_am()", size = 30) +
  theme_void()

ggsave("figs/art/Jane_Doe_02.png", width = size[1], height = size[2])

ggplot() +
  annotate("text", x = 1, y = 1, label = "u s e R", size = 20, color = "white") +
  theme_void() +
  theme(plot.background = element_rect(fill = '#0b6ec7'))

ggsave("figs/art/John_Doe_01.png", width = size[1], height = size[2])
